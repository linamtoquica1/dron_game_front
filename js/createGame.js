var app = new Vue({
  el: "#app",
  data: {
    player1: "",
    player2: "",
    startingZip: "",
    startingCity: "",
    endingZip: "",
    endingCity: ""
  },
  watch: {},
  methods: {
    start: function(event) {
      data = {
        player_1: {
          name: this.player1
        },
        player_2: {
          name: this.player2
        }
      };
      this.startApi(data);
    },

    startApi: _.debounce(function(data) {
      axios
        .post("http://localhost:8000/games/", data)
        .then(function(response) {
          console.log(response);
          localStorage.game_id = response.data.id;
          localStorage.setItem(
            "player1",
            JSON.stringify(response.data.player_1)
          );
          localStorage.setItem(
            "player2",
            JSON.stringify(response.data.player_2)
          );
          localStorage.player_turn = "player1";
          localStorage.player_round = 1;
          console.log(localStorage.player1, response.data.id);
          window.location.href = "/pages/game";
        })
        .catch(function(error) {
          console.log(error);
        });
    }, 500)
  }
});
