console.log(localStorage.player_round);
var movePlayer1 = "";
var movePlayer2 = "";

var app = new Vue({
  el: "#app",
  data: {
    player: "",
    player2: "",
    selected: "",
    round_number: localStorage.player_round,
    message: "",
    round_1: "",
    round_2: "",
    round_3: "",
    winner_round1: "",
    winner_round2: "",
    winner_round3: "",
    player1Name: JSON.parse(localStorage.player1).name,
    player2Name: JSON.parse(localStorage.player2).name,
    player1Score: "",
    player2Score: "",

    round_nums: [],
    winner_rounds: [],
    containerGame: null,
    containerGameAgain: null
  },
  watch: {},
  methods: {
    getPlayerName: _.debounce(function() {
      console.log(
        "aqui",
        localStorage.player_turn,
        localStorage.player1,
        localStorage.player2
      );
      player1 = JSON.parse(localStorage.player1);
      player2 = JSON.parse(localStorage.player2);
      if (localStorage.player_turn === "player1") {
        this.player = player1.name;
      } else {
        this.player = player2.name;
      }
      console.log("este es:", localStorage.player_round);
      if (localStorage.player_round > 3) {
        this.containerGame = false;
        this.containerGameAgain = true;
      } else {
        this.containerGame = true;
        this.containerGameAgain = false;
      }
    }),

    play: _.debounce(function(event) {
      if (localStorage.player_turn === "player1") {
        movePlayer1 = this.selected;
        localStorage.player_turn = "player2";
      } else {
        movePlayer2 = this.selected;
        localStorage.player_turn = "player1";
      }
      app.selected = "move option";
      app.message = "";
      this.getPlayerName();

      console.log(">>", movePlayer1, movePlayer2);
      if (movePlayer1 != "" && movePlayer2 != "") {
        data = {
          game: localStorage.game_id,
          player1_move: movePlayer1,
          player2_move: movePlayer2
        };
        if (localStorage.player_round <= 3) {
          this.playApi(data);
        } else {
        }
        movePlayer1 = "";
        movePlayer2 = "";
      }
    }),

    playApi: _.debounce(function(data) {
      axios
        .post("http://localhost:8000/play/", data)
        .then(function(response) {
          console.log(response.data);
          console.log(response.data.message, response.data.message);
          app.round_number = response.data.round;
          localStorage.player_round = response.data.round;
          app.message = response.data.message;
          app.player1Score = response.data.player_1_score;
          app.player2Score = response.data.player_2_score;

          if (app.message.includes("Continue playing next round")) {
            app.round_number = response.data.round + 1;
            app.roundApi();
            app.player1Score = "";
            app.player2Score = "";
          }
          if (app.message.includes("has won game")) {
            app.roundApi();

            app.containerGame = false;
            app.containerGameAgain = true;
            app.player1Score = "";
            app.player2Score = "";
            alert(app.message);
          }
        })
        .catch(function(error) {
          console.log(error);
        });
    }, 500),

    roundApi: _.debounce(function() {
      app.round_nums = [];
      app.winner_rounds = [];
      axios
        .get("http://localhost:8000/rounds/" + localStorage.game_id)
        .then(function(response) {
          console.log(response.data);
          response.data.forEach(function(valor, indice, array) {
            app.round_nums.push({ number: indice + 1 });
            app.winner_rounds.push({ name: app.searchPlayer(valor["winner"]) });

            console.log("En el índice " + indice + " hay este valor: " + valor);
          });
        })
        .catch(function(error) {
          console.log(error);
        });
    }, 500),

    searchPlayer: function(id) {
      player1 = JSON.parse(localStorage.player1);
      player2 = JSON.parse(localStorage.player2);
      if (player1.id === id) {
        return player1.name;
      } else if (player2.id == id) {
        return player2.name;
      }
    },
    playAgain: function() {
      window.location.href = "/";
    }
  },
  beforeMount() {
    this.roundApi();
    this.getPlayerName();
  }
});
